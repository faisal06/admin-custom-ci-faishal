<?php
    
    defined('BASEPATH') OR exit('No direct script access allowed');
    
    class Template
    {
        protected $ci;
    
        public function __construct()
        {
            $this->ci =& get_instance();
        }
        public function render($path, $data)
        {
            // echo "Ini Dari function test Template library";
            $data['head'] = $this->ci->load->view('layout/head',$data,TRUE);
            $data['preloader'] = $this->ci->load->view('layout/preloader',$data,TRUE);
            $data['header'] = $this->ci->load->view('layout/header',$data,TRUE);
            $data['main'] = $this->ci->load->view('layout/main',$data,TRUE);        
            $data['sidebar'] = $this->ci->load->view('layout/sidebar',$data,TRUE);    
            $data['footer'] = $this->ci->load->view('layout/footer',$data,TRUE);    
            $this->ci->load->view($path,$data);
        }
    }
    
    /* End of file Costum.php */
    
?>