<!DOCTYPE html>
<?=$head?>
<html dir="ltr" lang="en">
<body>
<?=$preloader?>
<div id="main-wrapper">
    <?=$header?>
    <?=$sidebar?>
    <?=$main?>
</div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <?=$footer?>
</body>

</html>